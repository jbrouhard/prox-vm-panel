<?php

/*

Proxmox VE APIv2 (PVE2) Client - PHP Class

Copyright (c) 2012-2014 Nathan Sullivan

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of 
the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions: 

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

*/



error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
// include configuration file

include (dirname(dirname(__FILE__)) . '/assets/config.inc.php'); // Include configuration
include (dirname(dirname(__FILE__)) . "/assets/password.php"); // include password_hash functionality

// Lets run the login paths

ob_start();

$uname = $_POST['username'];
$passwd = $_POST['password'];

// Protect against MySQL Injection
$uname = stripslashes($uname);
$uname = mysqli_real_escape_string($link,$uname);


// Hash the password
		
$passwd_hash = password_hash($passwd, PASSWORD_BCRYPT);

//Lets run the SQL
$sql = "SELECT username,password FROM member WHERE username = '$uname'";
$result = mysqli_query($link,$sql);
$row = mysqli_fetch_assoc($result);

$passwd_db = $row['password'];


// If result matched username and password, table row must be *ONE* row only
if (password_verify($passwd, $passwd_db)){
	session_start();
		
	$_SESSION['uname'] = $uname;
	
	// Log the event.
	
	header("Location:http://google.com");
	}
else{
	
	// Log the event.
	
	echo '<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Dark Login Form</title>
  <link rel="stylesheet" href="/assets/css/login.css">
  <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body><center></center><h1 class=login>Unauthorized Access<br><br>Wrong Username or Password</h1></center></body></html>';
	header('Refresh:5; url=index.php');
}

ob_end_flush();
