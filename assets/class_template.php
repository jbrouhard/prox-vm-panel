<?php

/*

Proxmox VE APIv2 (PVE2) Client - PHP Class

Copyright (c) 2012-2014 Nathan Sullivan

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of 
the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions: 

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

*/


class template 
{
    
    /*     Variables we will be using in this class
         template_dir - Simply for organisational reasons, the directory of out template files. 
        file_ext - For organisational reasons, the templae file extensions
        buffer - The contents of the given template file that we work with.
    */
    
    var $template_dir     = '.templates/';
    var $file_ext         = '.html';
    var $buffer;
    
    function buff_template ($file) 
    {
        if( file_exists( $this -> template_dir . $file . $this -> file_ext ) )
        {
            $this -> buffer = file_get_contents( $this -> template_dir . $file . $this -> file_ext );
            
            // For < PHP 4.3.0
            // $this -> buffer = join('', file( $this -> template_dir . $file . $this -> file_ext ));
            
            return $this -> buffer;
        }
        else
        {
            echo $this -> template_dir . $file . $this -> file_ext . ' does not exist';
        }
    }
    
    function parse_variables($input, $array)
    {    
            $search = preg_match_all('/{.*?}/', $input, $matches);
                        
            for($i = 0; $i < $search; $i++)
            {
                $matches[0][$i] = str_replace(array('{', '}'), null, $matches[0][$i]);
            
            }
            
            foreach($matches[0] as $value)
            {
                $input = str_replace('{' . $value . '}', $array[$value], $input);
            }
                 
        
            return $input;
    }

}
?>